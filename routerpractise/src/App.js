import React, {Component} from 'react';
import './App.scss';
import Preloader from "./components/Preloader/Preloader";
import {Link,Route} from "react-router-dom";
const User = () => <h1>User comp</h1>;
const Login = () => <h1>Login</h1>;
const Register = () => <h1>Register</h1>;

class App extends Component {
  render() {
    return (
      <>
          <h1>We need to download it</h1>
          <Link to={'/user'} >User</Link>
          <Link to={'/login'} >login</Link>
          <Link to={'/register'} >Register</Link>
          <Route path={"/user"} component={User} />
          <Route path={"/login"} component={Login} />
          <Route path={"/register"} component={Register} />
      </>
    );
  }
}

export default App;
