
import React, {Component} from 'react';
import {Link,NavLink,withRouter} from "react-router-dom";

class TabHead extends Component {
    constructor(props) {
        super(props);
        this.props.history.push('./page-web');

    }
    render() {
        return (
            <div>
                <ul className="tab-link-wrapper">
                    <li className="tab-link"><NavLink  to={'/page-web'}>Web</NavLink></li>
                    <li className="tab-link"><NavLink  to={'/page-design'}>Design</NavLink></li>
                    <li className="tab-link"><NavLink  to={'/page-marketing'}>Marketing</NavLink></li>
                </ul>
            </div>
        );
    }
}

export default withRouter(TabHead);