import React from 'react';
import {Route} from 'react-router-dom';
import TabHead from "./components/TabHead";
import Web from "./components/Web";
import Design from "./components/Design";
import Marketing from "./components/Marketing";
import './App.scss';
function App() {
    return (
            <div className="App">
                <TabHead/>
                <Route path={"/page-web"} exact component={Web}/>
                <Route path={"/page-design"} component={Design}/>
                <Route path={"/page-marketing"} component={Marketing}/>
            </div>
    )
}

export default App;