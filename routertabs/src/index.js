import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter } from 'react-router-dom';
import Hello from './App';

ReactDOM.render(<BrowserRouter><Hello /></BrowserRouter>, document.getElementById('root'));
